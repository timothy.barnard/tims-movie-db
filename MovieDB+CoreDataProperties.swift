//
//  MovieDB+CoreDataProperties.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//
//

import Foundation
import CoreData


extension MovieDB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieDB> {
        return NSFetchRequest<MovieDB>(entityName: "MovieDB")
    }
    
    public static func entityDescription(context: NSManagedObjectContext) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: String(describing: self), in: context)!
    }

    @NSManaged public var vote_count: NSDecimalNumber?
    @NSManaged public var id: Int32
    @NSManaged public var vote_average: NSDecimalNumber?
    @NSManaged public var poster_path: String?
    @NSManaged public var original_language: String?
    @NSManaged public var backdrop_path: String?
    @NSManaged public var overview: String?
    @NSManaged public var release_date: NSDate?
    @NSManaged public var title: String?
    @NSManaged public var genres: String?
    @NSManaged public var status: String?
    @NSManaged public var runtime: Int32

}
