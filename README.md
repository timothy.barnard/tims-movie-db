# The Movie Database iOS app written in Swift 5

It has the following features:
1. Shows the list of popular movies
2. Detail page with option to play the trailer 
3. Search for a movie offline and online
4. Stores the movies for offline use

## This project has used Carthage package manager with the following packages used:
```
- Alamofire/Alamofire for network requests 
- YoutubeDirectLinkExtractor to convert a youtube link to play using AVPlayer
```

## VIPER Design Pattern 
![alt text](https://cdn-images-1.medium.com/max/1200/1*6W73TuYu1DWi9JY4_Uh8aA.png)
⋅⋅⋅This project has used the VIPER design pattern. It implements the separation of concern paradigm by  
⋅⋅⋅separating the view, logic, network and database requests and app navigation. 
[VIPER Tutorial ](https://medium.com/@smalam119/viper-design-pattern-for-ios-application-development-7a9703902af6)
