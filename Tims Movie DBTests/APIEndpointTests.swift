//
//  APIEndpointTests.swift
//  Tims Movie DBTests
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import XCTest
@testable import Tims_Movie_DB

class APIEndpoint_Tests: XCTestCase {
    
    func test_image() {
        let iamgeEndpoint = APIEndPoints.image("image123.png")
        XCTAssertEqual(iamgeEndpoint.path, "image123.png")
        XCTAssertEqual(iamgeEndpoint.urlString, "https://image.tmdb.org/t/p/w500/image123.png")
    }
    
    func test_popularMovies() {
        let popularMovieEndpoint = APIEndPoints.popularMovies(2)
        XCTAssertEqual(popularMovieEndpoint.path, "/3/movie/popular")
        XCTAssertEqual(popularMovieEndpoint.queryItems.count, 3)
        let pageQueryItem = popularMovieEndpoint.queryItems.first(where: { $0.name == "page" })?.value
        XCTAssertEqual(pageQueryItem, "2")
    }
    
    func test_searchMovie() {
        let searchMovieEndpoint = APIEndPoints.search("Tims Movie")
        XCTAssertEqual(searchMovieEndpoint.path, "/3/search/movie")
        XCTAssertEqual(searchMovieEndpoint.queryItems.count, 3)
        let searchQueryItem = searchMovieEndpoint.queryItems.first(where: { $0.name == "query" })?.value
        XCTAssertEqual(searchQueryItem, "Tims Movie")
    }
    
    func test_movieVideos() {
        let movieVideosEndpoint = APIEndPoints.movieVideos(15)
        let token = movieVideosEndpoint.token
        let language = Locale.current.languageCode!
        XCTAssertEqual(movieVideosEndpoint.path, "/3/movie/15/videos")
        XCTAssertEqual(movieVideosEndpoint.urlString,
                       "https://api.themoviedb.org/3/movie/15/videos?api_key=\(token)&language=\(language)")
    }
    
    func test_movieDetail() {
        let movieDetailEndpoint = APIEndPoints.movieDetail(15)
        XCTAssertEqual(movieDetailEndpoint.path, "/3/movie/15")
        XCTAssertEqual(movieDetailEndpoint.queryItems.count, 2)
    }
}
