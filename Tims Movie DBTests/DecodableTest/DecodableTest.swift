//
//  DecodableTest.swift
//  Tims Movie DBTests
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import XCTest

@testable import Tims_Movie_DB

class Decodable_Tests: XCTestCase {
    
    var data: Data?
    
    override func setUp() {
        super.setUp()
        guard let url = Bundle(for: type(of: self)).url(forResource: "test", withExtension: "json") else {
            print("File does not exist")
            return
        }
        self.data = try? Data(contentsOf: url)
    }
    
    override func tearDown() {
        self.data = nil
    }
    
    func test_data() {
        XCTAssertNotNil(self.data)
    }
    
    func test_MovieDecoding() {
        let movieMeta: MovieMeta? = MovieMeta.decodeData(self.data!)
        XCTAssertNotNil(movieMeta)
        XCTAssertEqual(movieMeta?.results?.count, 3)
        XCTAssertEqual(movieMeta?.results?.first?.title, "Titanic")
    }
    
    func test_emptyDate() {
        let movieMeta: MovieMeta? = MovieMeta.decodeData(self.data!)
        let foundMovie = movieMeta?.results?.first(where: { $0.title == "Titanic bad date" })
        XCTAssertNotNil(foundMovie)
        XCTAssertNil(foundMovie?.releaseDate)
    }
    
    func test_movieDate() {
        let movieMeta: MovieMeta? = MovieMeta.decodeData(self.data!)
        let foundMovie = movieMeta?.results?.first(where: { $0.id == 597 })
        XCTAssertNotNil(foundMovie)
        XCTAssertNotNil(foundMovie?.releaseDate)
        XCTAssertEqual(foundMovie?.releaseDate?.description, "1997-11-17 23:00:00 +0000")
    }
    
    func test_RatingStars() {
        let movieMeta: MovieMeta? = MovieMeta.decodeData(self.data!)
        let firstMovie = movieMeta?.results?.first
        XCTAssertNotNil(firstMovie)
        XCTAssertEqual(firstMovie?.voteAverage.ratingStars, "⭐️⭐️⭐️⭐️⭐️⭐️⭐️")
    }
}

