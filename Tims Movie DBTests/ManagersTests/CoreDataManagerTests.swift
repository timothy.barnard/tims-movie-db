//
//  CoreDataManagerTests.swift
//  Tims Movie DBTests
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import XCTest
import CoreData

class MovieCoreDataManager_Tests: XCTestCase {
    
    let mockPersistanceContainer = MockPersistanceContainer()
    var coreDataManager = CoreDataManager.shared
    
    func test_coreDataManagerInstance() {
        XCTAssertNotNil(coreDataManager)
    }
    
    func test_coreDataContext() {
        XCTAssertNotNil(mockPersistanceContainer.mockPersistantContainer)
        XCTAssertNotNil(mockPersistanceContainer.managedObjectModel)
    }
    
    func test_checkEmpty() {
        let movieEntity: NSEntityDescription = MovieDB.entityDescription(
            context: mockPersistanceContainer.mockPersistantContainer.viewContext)
        let movie = MovieDB(entity: movieEntity, insertInto: mockPersistanceContainer.mockPersistantContainer.viewContext)
        movie.id = 9
        movie.title = "Tims movie"
        movie.overview = "This is an amazing movie"
        movie.genres = "Fun, Exciting"
        movie.release_date = NSDate()
        movie.status = "Live"
        movie.vote_average = 8
        let result = coreDataManager.saveContext(mockPersistanceContainer.mockPersistantContainer.viewContext)
        XCTAssertTrue(result)
    }
    
    func test_retrievingMovie() {
        let movieEntity: NSEntityDescription = MovieDB.entityDescription(
            context: mockPersistanceContainer.mockPersistantContainer.viewContext)
        let movie = MovieDB(entity: movieEntity, insertInto: mockPersistanceContainer.mockPersistantContainer.viewContext)
        movie.id = 8
        movie.title = "Tims movie"
        movie.overview = "This is an amazing movie"
        movie.genres = "Fun, Exciting"
        movie.release_date = NSDate()
        movie.status = "Not Live"
        movie.vote_average = 6
        let result = coreDataManager.saveContext(mockPersistanceContainer.mockPersistantContainer.viewContext)
        XCTAssertTrue(result)
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "MovieDB")
        let results = try? mockPersistanceContainer.mockPersistantContainer.viewContext.fetch(request)
        XCTAssertNotNil(results)
        XCTAssertTrue(results?.count == 1)
    }
}
