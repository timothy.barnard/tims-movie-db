//
//  MovieLocalStoreTest.swift
//  Tims Movie DBTests
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import CoreData
import XCTest

@testable import Tims_Movie_DB

class MovieLocalStore_Test: XCTestCase {
    
    var mockPersistanceContainer: MockPersistanceContainer!
    var localMovieStore: LocalMovieStore!
    
    override func setUp() {
        super.setUp()
        mockPersistanceContainer = MockPersistanceContainer()
        localMovieStore = LocalMovieStore(mockPersistanceContainer.context)
        let movieOne: Movie = {
            var movie = Movie()
            movie.id = 3
            movie.title = "Tims Movie"
            movie.overview = "Welcome to my movie"
            movie.voteAverage = 8
            movie.releaseDate = Date()
            return movie
        }()
        let movieTwo: Movie = {
            var movie = Movie()
            movie.id = 4
            movie.title = "Tims Movie Two"
            movie.overview = "Welcome to my movie two"
            movie.voteAverage = 8
            movie.releaseDate = Date()
            return movie
        }()
        
        localMovieStore.saveMovieList([movieOne, movieTwo])
    }
    
    func test_retrievingMovies() {
        let movies = localMovieStore.getListOfMovies()
        XCTAssertNotNil(movies)
        XCTAssertTrue(movies?.count == 2)
        XCTAssertEqual(movies?.first(where: { $0.id == 3 })?.title, "Tims Movie")
    }
    
    
    func test_searchMovies() {
        let movies = localMovieStore.searchForMovie("Tims Movie Two")
        XCTAssertNotNil(movies)
        XCTAssertTrue(movies?.count == 1)
        XCTAssertEqual(movies?.first?.title, "Tims Movie Two")
    }
}
