//
//  MovieVideoRequest.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

struct MovieVideoMeta: Decodable {
    var id: Int?
    var results: [MovieVideo]?
}

struct MovieVideo: Decodable {
    var id: String?
    var key: String?
    var name: String?
    var site: String?
    var size: Int?
    var type: String?
    
    var youtubeURL: URL? {
        guard let key = self.key, site == "YouTube" else {
            return nil
        }
        return URL(string: "https://www.youtube.com/watch?v=\(key)")
    }
}
