//
//  PopularMovie.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

struct MovieMeta: Decodable {
    var page: Int?
    var totalResults: Int?
    var totalPages: Int?
    var results: [Movie]?
}

struct Genre: Decodable {
    var id: Int?
    var name: String?
}

struct MovieDetail: Decodable {
    var backdropPath: String?
    var genres: [Genre]?
    var runtime: Int?
    var status: String?
    
    var genresStr: String?
    
    var labelGenre: String {
        if (!(genres ?? []).isEmpty) {
            return (genres ?? []).compactMap{ $0.name }.joined(separator: ", ")
        } else {
           return self.genresStr ?? "--"
        }
    }
}

struct Movie: Decodable {
    var voteCount: Int?
    var voteAverage: Double?
    var id: Int?
    var video: Bool?
    var title: String?
    var posterPath: String?
    var originalLanguage: String?
    var overview: String?
    var releaseDate: Date?
    
    var detail: MovieDetail?
    
    init() {}
    
    enum CodingKeys: String, CodingKey {
        case voteCount, voteAverage, id, video, title, overview
        case posterPath, originalLanguage, releaseDate
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.voteCount = container.decodeWrapper(.voteCount)
        self.voteAverage = container.decodeWrapper(.voteAverage)
        self.id = container.decodeWrapper(.id)
        self.video = container.decodeWrapper(.video)
        self.title = container.decodeWrapper(.title)
        self.posterPath = container.decodeWrapper(.posterPath)
        self.originalLanguage = container.decodeWrapper(.originalLanguage)
        self.overview = container.decodeWrapper(.overview)
        self.releaseDate = container.decodeWrapper(.releaseDate)
    }
}
