//
//  Int+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

internal extension Optional where Wrapped == Int {
    
    /// Converts minutes to hours and mins in string form
    var minsToHoursMins: String {
        guard let mins = self else {return "---"}
        let doubleMins = Double(mins)
        let hours = String(Int(doubleMins / 60))
        let min = String(Int(doubleMins.truncatingRemainder(dividingBy: 60)))
        return "\(hours)h \(min)min"
    }
}
