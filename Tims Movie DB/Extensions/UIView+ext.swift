//
//  UIView+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UIView {
    
    /// Pins all edges of the view passed in to the main view with no constants
    ///
    /// - Parameter toView: view to pin to the main view
    func pinToEdges(_ toView: UIView) {
        NSLayoutConstraint.activate([
            self.safeAreaLayoutGuide.topAnchor.constraint(equalTo: toView.topAnchor),
            self.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: toView.bottomAnchor),
            self.safeAreaLayoutGuide.leadingAnchor.constraint(equalTo: toView.leadingAnchor),
            self.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: toView.trailingAnchor)
            ])
    }
    
    /// Addes rounded corners with radius of 10
    func addRoundedEdges() {
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
    /// Adds blur view to the main view. it auto resizes to the main view
    ///
    /// - Parameters:
    ///   - style: UIBlurEffect.Style i.e light
    ///   - alpha: Transparency of the view
    func addBlurr(_ style: UIBlurEffect.Style, alpha: CGFloat) {
        let effect = UIBlurEffect(style: style)
        let effectView = UIVisualEffectView(effect: effect)
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        self.addSubview(effectView)
    }
    
    /// Adds shadow with color black and offset of zero
    func addShadow() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    }
    
}
