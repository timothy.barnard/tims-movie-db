//
//  UIImageView+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UIImageView {
    
    /// Load Image from the web
    ///
    /// - Parameters:
    ///   - url: Image path
    ///   - contentMode: ContentMode default to scaleAspectFit
    func loadIamge(with url: URL, contentMode: UIImageView.ContentMode = UIImageView.ContentMode.scaleAspectFit) {
        let imageName = url.fileName
        let fileManager = ImageFileManager()
        if let imagePath = fileManager.getImagPath(imageName) {
            self.image = UIImage(contentsOfFile: imagePath)
        }
        URLSession.shared.dataTask(with: url) { data, respsonse, error in
            guard let data = data, let mimeType = respsonse?.mimeType,  mimeType.hasPrefix("image"), let image = UIImage(data: data) else {return}
            fileManager.saveImageToFile(imageName, image: image)
            DispatchQueue.main.async { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    
    /// Initialier to default image with a placeholder
    /// and set the autoresizeing to false
    ///
    /// - Parameter placeHolder: String
    convenience init(_ placeHolder: String) {
        self.init()
        frame = .zero
        image = UIImage(named: placeHolder)
        translatesAutoresizingMaskIntoConstraints = false
    }
}
