//
//  UIDevice+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UIDevice {
    
    /// If the Devices orietation is is valid then it uses the isLandscape
    /// else it uses the UIApplication isLandscape
    static var isLandscape: Bool {
        return UIDevice.current.orientation.isValidInterfaceOrientation
            ? UIDevice.current.orientation.isLandscape
            : UIApplication.shared.statusBarOrientation.isLandscape
    }
   
    /// If the Devices orietation is is valid then it uses the isPortrait
    /// else it uses the UIApplication isPortrait
    static var isPortrait: Bool {
        return UIDevice.current.orientation.isValidInterfaceOrientation
            ? UIDevice.current.orientation.isPortrait
            : UIApplication.shared.statusBarOrientation.isPortrait
    }
}
