//
//  URL+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

internal extension URL {
    var fileName: String {
        return self.deletingPathExtension().lastPathComponent
    }
}
