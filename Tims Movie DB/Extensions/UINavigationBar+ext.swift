//
//  UINavigationBar+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UINavigationBar {

    /// Set the navigation bar transparent with the tint and title to white
    func setBarTransparent() {
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
        isTranslucent = true
        tintColor = .white
        titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
    }
    
    /// Sets the navigation bar to black wtih tint and title to white
    func setBarDark() {
        tintColor = .white
        barTintColor = .black
        titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
    }
}
