//
//  Double+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

internal extension Optional where Wrapped == Double {
    
    /// Returns a number of ⭐️'s based on the given value
    var ratingStars: String {
        let rating = Int(self ?? 0)
        return String(repeating: "⭐️", count: rating)
    }
}
