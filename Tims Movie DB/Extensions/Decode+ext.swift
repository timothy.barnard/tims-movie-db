//
//  Decode+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

public extension Decodable {
    
    /// Decodable function that returns back the object
    ///
    /// - Parameters:
    ///   - data: Data in which to decode
    ///   - dateForamt: the format of the date
    /// - Returns: Optioanl T
    static func decodeData<T: Decodable>(_ data: Foundation.Data, dateForamt: String = "yyyy-MM-dd") -> T? {
        do {
            let jsonDecoder: JSONDecoder = {
                let formatter = DateFormatter()
                formatter.dateFormat = dateForamt
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = JSONDecoder.DateDecodingStrategy.formatted(formatter)
                return decoder
            }()
            let object = try jsonDecoder.decode(T.self, from: data)
            return object
        } catch {
            print(error)
        }
        return nil
    }
}


internal extension KeyedDecodingContainer {
        
    /// Helper to decode an object if that value exists
    ///
    /// - Parameter key: CodingKeys option
    /// - Returns: Optional value
    func decodeWrapper<T: Decodable>(_ key: K) -> T? {
        do {
            return try decodeIfPresent(T.self, forKey: key)
        } catch {
            print(error)
            return nil
        }
    }
}
