//
//  Date+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

internal extension Date {
    
    enum DateError: Error {
        case invalidDate
    }
    
    /// Creates a date from a string, if not valid then an error is thrown
    ///
    /// - Parameters:
    ///   - dateString: date in which to convert
    ///   - format: format of the date defautls to `yyyy-MM-dd`
    /// - Returns: Date object
    /// - Throws: DateError of invalid
    static func new(_ dateString: String, format: String = "yyyy-MM-dd") throws -> Date {
        let formatter = DateFormatter.current
        formatter.dateFormat = format
        if let date =  formatter.date(from: dateString) {
            return date
        }
        throw DateError.invalidDate
    }
}


internal extension DateFormatter {
    
    /// Creates a new formatter with current locale, timezone and calendar
    static var current: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = .current
        formatter.timeZone = .current
        formatter.calendar = .current
        return formatter
    }
}
