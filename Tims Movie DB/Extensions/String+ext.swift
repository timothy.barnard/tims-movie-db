//
//  String+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 24/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
