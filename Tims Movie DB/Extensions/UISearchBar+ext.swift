//
//  UISearchBar+ext.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UISearchBar {
    
    /// Sets the textField background color
    func setTextFieldBackgroundColor(_ color: UIColor = UIColor.darkGray) {
        if let textField = self.value(forKey: "searchField") as? UITextField {
            textField.backgroundColor = color
        }
    }
}
