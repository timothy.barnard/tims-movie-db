//
//  UILabel.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UILabel {
    
    /// Initializer using system font, text alligin
    /// to left and autoresizing contraints to false
    ///
    /// - Parameters:
    ///   - fontSize: CGFloat
    ///   - color: UIColor of the text
    convenience init(fontSize: CGFloat, color: UIColor) {
        self.init(frame: .zero)
        text = "----"
        font = UIFont.systemFont(ofSize: fontSize)
        textColor = color
        textAlignment = NSTextAlignment.left
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    /// Creates a title label with font size of 30
    /// System font, text alignment to left
    ///
    /// - Parameters:
    ///   - numberOfLines: Int defualt to 1
    ///   - textColor: UIColor default to black
    /// - Returns: UILabel
    static func createTitle(_ numberOfLines: Int = 1, textColor: UIColor = .black) -> UILabel {
        let label = UILabel(fontSize: 30, color: textColor)
        label.numberOfLines = numberOfLines
        return label
    }
    
    /// Creates a sub title label with font size 17
    ///
    /// - Parameters:
    ///   - numberOfLines: Int defualt to 1
    ///   - textColor: UIColor default to black
    /// - Returns: UILabel
    static func createSubTitle(_ numberOfLines: Int = 1, textColor: UIColor = .black) -> UILabel {
        let label = UILabel(fontSize: 17, color: textColor)
        label.numberOfLines = numberOfLines
        return label
    }
    
    /// Creates a subheading with font size of 15
    ///
    /// - Parameters:
    ///   - numberOfLines: Int defualt to 1
    ///   - textColor: UIColor default to black
    /// - Returns: UILabel
    static func createSubHeading(_ numberOfLines: Int = 2, textColor: UIColor = .white) -> UILabel {
        let label = UILabel(fontSize: 15, color: textColor)
        label.numberOfLines = numberOfLines
        return label
    }
    
    /// Create small label with font size of 12
    ///
    /// - Parameter textColor: UIColor default to black
    /// - Returns: UILabel
    static func createSmall(_ textColor: UIColor = .lightGray) -> UILabel {
        let label = UILabel(fontSize: 12, color: textColor)
        label.textAlignment = NSTextAlignment.right
        return label
    }
}
