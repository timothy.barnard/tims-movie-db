//
//  UITextView.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UITextView {
    
    /// Creates an empty text area, that is disabled, font size of 15, text color white
    /// and background color set to clear
    ///
    /// - Returns: UITextView
    static func createTextArea() -> UITextView {
        let textView = UITextView(frame: .zero)
        textView.backgroundColor = .clear
        textView.isUserInteractionEnabled = false
        textView.textColor = .white
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        return textView
    }
}

