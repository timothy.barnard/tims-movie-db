//
//  UIImageView.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal extension UIImageView {
    
    /// Make an imageview with `default_movie` image
    ///
    /// - Parameter withBlue: add blur view
    /// - Returns: UIImageView
    static func makeMovieImageView(_ withBlue: Bool = false) -> UIImageView {
        let imageView = UIImageView("default_movie")
        imageView.isUserInteractionEnabled = true
        if withBlue {imageView.addBlurr(.dark, alpha: 0.7)}
        return imageView
    }
    
    /// Creates a movie iamgeView with `default_movie` asset image
    /// without rounded edges
    ///
    /// - Returns: UIImageView
    static func makeMovieRoundedEdges() -> UIImageView {
        let imageView = UIImageView.makeMovieImageView()
        imageView.addRoundedEdges()
        return imageView
    }
}
