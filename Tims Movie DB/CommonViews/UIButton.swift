//
//  UIButton.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

extension UIButton {
    
    /// Creates a play button with `play` image asset without a title
    ///
    /// - Returns: UIButton
    static func makePlayButton() -> UIButton {
        let button = UIButton(frame: .zero)
        button.setImage(UIImage(named: "play"), for: .normal)
        button.setTitle(nil, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}
