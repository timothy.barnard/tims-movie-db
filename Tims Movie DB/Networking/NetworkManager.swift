//
//  NetworkManager.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import Alamofire
import Network

/// Network Manager network layer
class NetworkManager: NSObject {
    
    var isOffline: Bool {
        return !(NetworkReachabilityManager()?.isReachable ?? false)
    }
    
    /// Retrieves data from the given URL passed in
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - retrievedData: callback with Optional Data and Error
    func getJSONData(_ url: URL, retrievedData: @escaping (_ data: Data?, _ error: Error?) -> Void ) {
        AF.request(url).responseJSON { (response) in
            retrievedData(response.data, response.error)
        }
    }
}
