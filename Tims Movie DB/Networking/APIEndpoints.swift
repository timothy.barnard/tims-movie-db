//
//  Constants.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

// Case for each endpoint
enum APIEndPoints {
    case popularMovies(Int)
    case image(String)
    case movieDetail(Int)
    case movieVideos(Int)
    case search(String)
}

extension APIEndPoints: EndPointType {
    
    /// Retrieving the token from the Info.plist file
    var token: String {
        guard let tmdbToken = Bundle.main.object(forInfoDictionaryKey: "TMDB_API_TOKEN") as? String else {
            fatalError("The Movie DB token is missing")
        }
        return tmdbToken
    }
    
    var baseURL: URL {
        var url: URL?
        switch self {
        case .popularMovies, .movieDetail,
             .movieVideos, .search: url = URL(string: "https://api.themoviedb.org")
        case .image: url = URL(string: "https://image.tmdb.org/t/p/w500")
        }
        guard let baseURL = url else { fatalError("Base UR could not set")}
        return baseURL
    }
    
    var path: String {
        switch self {
        case .popularMovies: return "/3/movie/popular"
        case .image(let path): return path
        case .movieDetail(let id): return "/3/movie/\(id)"
        case .movieVideos(let id): return "/3/movie/\(id)/videos"
        case .search: return "/3/search/movie"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .popularMovies, .image, .search,
             .movieDetail, .movieVideos: return .get
        }
    }
    
    /// Build the URL string using URLComponents
    var urlString: String {
        guard var urlComponents = URLComponents(url: self.baseURL, resolvingAgainstBaseURL: true) else {fatalError("Could not create URLComponent")}
        switch self {
        case .popularMovies:
            urlComponents.path = self.path
            urlComponents.queryItems = self.queryItems
        case .movieDetail, .movieVideos:
            urlComponents.path = self.path
            urlComponents.queryItems = self.queryItems
        case .image:
            return self.baseURL.appendingPathComponent(self.path).absoluteString
        case .search:
            urlComponents.path = self.path
            urlComponents.queryItems = self.queryItems
        }
        return urlComponents.string!
    }
    
    /// Setting up the URLQueryItems for each endpoint including the
    /// basic two, api_key and language
    var queryItems: [URLQueryItem] {
        var items = [
            URLQueryItem(name: "api_key", value: self.token),
            URLQueryItem(name: "language", value: Locale.current.languageCode)
        ]
        switch self {
        case .popularMovies(let page):
            items.append(URLQueryItem(name: "page", value: "\(page)"))
        case .search(let query):
            items.append(URLQueryItem(name: "query", value: query))
        default: break
        }
        return items
    }
    
    var url: URL? {
        return URL(string: self.urlString)
    }
}
