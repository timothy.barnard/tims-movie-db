//
//  MovieService.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

// MovieService class for retieve moves
class MovieService: NSObject {
    
    let networkManager = NetworkManager()
    
    /// Gets a list of movies from the API
    /// If there is no network connection, it will try read from the database
    /// Once a sucessfull result is made and the current page is 1 then it resets the database.
    /// and adds the current list
    ///
    /// - Parameters:
    ///   - page: Current page
    ///   - retrievedMovies: callback with Optional list of Movie
    internal func getListOfPopularMovies(_ page: Int, retrievedMovies: @escaping (_ movies: [Movie]?) -> Void ) {
        let movieLocalStore = LocalMovieStore()
        if networkManager.isOffline {
            if let movies = movieLocalStore.getListOfMovies() {
                retrievedMovies(movies)
            }
        }
        guard let url = APIEndPoints.popularMovies(page).url else {return}
        networkManager.getJSONData(url) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {return}
            guard let movieMeta: MovieMeta = MovieMeta.decodeData(data) else {return}
            if page == 1 { // If the user reloads page one, then refreshed store data
                movieLocalStore.removeAll()
            }
            movieLocalStore.saveMovieList(movieMeta.results)
            retrievedMovies(movieMeta.results)
        }
    }
    
    /// Gets movie details
    ///
    /// - Parameters:
    ///   - id: movie id
    ///   - retreviedMovie: callback with Optioanal MovieDetail
    internal func getMovieDetails(_ id: Int, retreviedMovie: @escaping (_ movie: MovieDetail?) -> Void) {
        guard let url = APIEndPoints.movieDetail(id).url else {return}
        networkManager.getJSONData(url) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {return}
            let movieDetail: MovieDetail? = MovieDetail.decodeData(data)
            if let detail = movieDetail {
                let localMovieStore = LocalMovieStore()
                localMovieStore.updateMovie(with: id, detail: detail)
            }
            retreviedMovie(movieDetail)
        }
    }
    
    /// Retrieves list of movie videos
    ///
    /// - Parameters:
    ///   - id: movie Id
    ///   - retreviedMovieVideos: callback with Optional list of MovieVideo
    internal func getMovieVideos(_ id: Int, retreviedMovieVideos: @escaping (_ movieVideos: [MovieVideo]?) -> Void) {
        guard let url = APIEndPoints.movieVideos(id).url else {return}
        networkManager.getJSONData(url) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {return}
            let movieVideoMeta: MovieVideoMeta? = MovieVideoMeta.decodeData(data)
            retreviedMovieVideos(movieVideoMeta?.results)
        }
    }
    
    
    /// Searches for a movie
    /// If there is no interent connection, then it searches in the database
    ///
    /// - Parameters:
    ///   - query: String in which to search
    ///   - retrievedMovies: callback with Optional list of Movie
    internal func searchForMovie(with query: String, retrievedMovies: @escaping(_ movies: [Movie]?) -> Void ) {
        if networkManager.isOffline {
            let movieLocalStore = LocalMovieStore()
            let foundMovies = movieLocalStore.searchForMovie(query)
            retrievedMovies(foundMovies)
        }
        guard let url = APIEndPoints.search(query).url else {return}
        networkManager.getJSONData(url) { (data, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {return}
            let movieMeta: MovieMeta? = MovieMeta.decodeData(data)
            retrievedMovies(movieMeta?.results)
        }
    }
}
