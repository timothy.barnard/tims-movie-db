//
//  EndPointType.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

internal enum HTTPMethod: String {
    case get = "GET"
}

internal protocol EndPointType {
    var baseURL: URL {get}
    var path: String {get}
    var httpMethod: HTTPMethod {get}
    var urlString: String {get}
    var url: URL? {get}
}
