//
//  PopularMoviesPresenter.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class PopularMoviesPresenter {

    // MARK: Properties
    var view: PopularMoviesView?
    var router: PopularMoviesWireframe?
    var interactor: PopularMoviesUseCase?
    
    var currentPage: Int = 1
    var alreadyLoaded: Bool = false
}

extension PopularMoviesPresenter: PopularMoviesPresentation {
    
    func viewDidLoad() {
        self.interactor?.getAllMovies(currentPage)
    }
    
    func viewDidAppear() {}
    
    func getNextPage(_ reset: Bool) {
        if reset {self.currentPage = 0}
        self.currentPage += 1
        self.interactor?.getAllMovies(currentPage)
    }
    
    func movieSelected(_ popularMovie: Movie, imageLoader: ImageLoader?) {
        self.router?.showMovieDetailPage(popularMovie, imageLoader: imageLoader)
    }
    
    func searchForMovie(_ query: String) {
        self.interactor?.searchForMovie(query)
    }
}

extension PopularMoviesPresenter: PopularMoviesInteractorOutput {
    
    func sendBackMovies(_ popularMovies: [Movie]?) {
        guard let popularMovies = popularMovies else {
            self.view?.showAlert("error".localized, message: "error-pull-to-refresh".localized)
            return
        }
        if self.currentPage > 1 {
            self.view?.updateList(popularMovies)
        } else {
            self.view?.refreshList(popularMovies, showNewData: false)
        }
        self.alreadyLoaded = true
    }
    
    func sendBackSearchedMovies(_ movies: [Movie]?, searchValue: String) {
        guard let searchedMovies = movies, !searchedMovies.isEmpty else {
            let noResultsStr = "no-results".localized
            self.view?.showAlert("error".localized, message: "\(noResultsStr): \(searchValue)")
            return
        }
        self.view?.refreshList(searchedMovies, showNewData: false)
    }
}
