//
//  PopularMoviesContract.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

/// Protcol for the presenter to communicate with the view
protocol PopularMoviesView {
    func updateList(_ popularMovies: [Movie])
    func refreshList(_ movies: [Movie], showNewData: Bool)
    func showAlert(_ title: String?, message: String)
}

/// Protcol for the view to communicate with the presenter
protocol PopularMoviesPresentation: class {
    func viewDidLoad()
    func viewDidAppear()
    func getNextPage(_ reset: Bool)
    func movieSelected(_ popularMovie: Movie, imageLoader: ImageLoader?)
    func searchForMovie(_ query: String)
}

/// Protcol for the presenter to communicate with the interactor
protocol PopularMoviesUseCase: class {
    func getAllMovies(_ page: Int)
    func searchForMovie(_ query: String)
}

/// Protcol for the interactor to communicate with the presenter
protocol PopularMoviesInteractorOutput: class {
    func sendBackMovies(_ popularMovies: [Movie]?)
    func sendBackSearchedMovies(_ movies: [Movie]?, searchValue: String)
}

/// Protcol for the presenter to communicate with the router
protocol PopularMoviesWireframe: class {
    func showMovieDetailPage(_ popularMovie: Movie, imageLoader: ImageLoader?)
}
