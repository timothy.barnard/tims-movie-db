//
//  PopularMoviesInteractor.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class PopularMoviesInteractor {

    // MARK: Properties
    weak var output: PopularMoviesInteractorOutput?
}

extension PopularMoviesInteractor: PopularMoviesUseCase {
    
    func getAllMovies(_ page: Int) {
        let movieService = MovieService()
        movieService.getListOfPopularMovies(page) { [weak self] (movies) in
            self?.output?.sendBackMovies(movies)
        }
    }
    
    func searchForMovie(_ query: String) {
        let movieService = MovieService()
        movieService.searchForMovie(with: query) { [weak self] (movies) in
            self?.output?.sendBackSearchedMovies(movies, searchValue: query)
        }
    }
}
