//
//  PopularMoviesRouter.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class PopularMoviesRouter {

    // MARK: Properties
    weak var viewController: UIViewController?

    // MARK: Static methods
    static func assembleModule() -> PopularMoviesViewController {
        let view = PopularMoviesViewController()
        let presenter = PopularMoviesPresenter()
        let router = PopularMoviesRouter()
        let interactor = PopularMoviesInteractor()

        view.presenter =  presenter

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor

        interactor.output = presenter
        
        router.viewController = view

        return view
    }
}

extension PopularMoviesRouter: PopularMoviesWireframe {
    
    func showMovieDetailPage(_ popularMovie: Movie, imageLoader: ImageLoader?) {
        let movieDetailViewController =
            MovieDetailRouter.assembleModule(popularMovie, imageLoader: imageLoader)
        self.viewController?.navigationController?.pushViewController(
            movieDetailViewController, animated: true)
    }
}
