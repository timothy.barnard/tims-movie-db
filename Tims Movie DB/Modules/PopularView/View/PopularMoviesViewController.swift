//
//  PopularMoviesViewController.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class PopularMoviesViewController: UIViewController {
    
    lazy var spinner: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        return activityIndicator
    }()

    // MARK: View Properties
    lazy var collectionView: UICollectionView = {
        let refreshControl = UIRefreshControl(frame: .zero)
        refreshControl.tintColor = .white
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: .valueChanged)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 100, height: 100)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: MovieCollectionViewCell.idenitifer)
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = .black
        collectionView.dataSource = self
        collectionView.refreshControl = refreshControl
        collectionView.delegate = self
        collectionView.prefetchDataSource = self
        collectionView.isScrollEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    lazy var searchBarButtonItem: UIBarButtonItem = {
        return UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.showSearchBar))
    }()
    lazy var newDataLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        label.center = CGPoint(x: self.view.frame.width / 2, y: -100)
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .center
        label.text = "Refresh to get updated list"
        label.addRoundedEdges()
        label.textColor = .white
        label.backgroundColor = UIColor.blue
        return label
    }()

    // MARK: Properties
    var presenter: PopularMoviesPresentation?
    private var popularMovies: [Movie] = []
    private var imageLoader: ImageLoader?

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        presenter?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.setBarDark()
        presenter?.viewDidAppear()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(
            alongsideTransition: { _ in self.collectionView.collectionViewLayout.invalidateLayout() },
            completion: { _ in }
        )
    }

    private func setupView() {
        self.imageLoader = ImageLoader()
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
        self.view.addSubview(collectionView)
        self.view.pinToEdges(self.collectionView)
        self.showSpinner()
    }
    
    @objc private func showSearchBar(_ sender: UIBarButtonItem) {
        let searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.setTextFieldBackgroundColor()
        self.navigationItem.titleView = searchBar
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelSearchBar))
        self.navigationItem.rightBarButtonItem = cancelButton
        searchBar.becomeFirstResponder()
    }
    
    @objc private func cancelSearchBar(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.navigationItem.titleView = nil
        self.navigationItem.rightBarButtonItem = searchBarButtonItem
        self.presenter?.getNextPage(true)
    }
    
    @objc private func refreshPage(_ sender: UIRefreshControl) {
        self.hideNewDataAlert()
        self.presenter?.getNextPage(true)
    }
    
    private func showNewDataAlert() {
        self.view.addSubview(newDataLabel)
        UIView.animate(withDuration: 0.8) {
            self.newDataLabel.center = CGPoint(x: self.view.frame.width / 2, y: 120)
        }
    }
    
    private func hideNewDataAlert() {
        UIView.animate(withDuration: 1.2, animations: { [unowned self] in
            self.newDataLabel.center = CGPoint(x: self.view.frame.width / 2, y: -100)
        }, completion: { [weak self] (_) in
            self?.newDataLabel.removeFromSuperview()
        })
    }
    
    private func showSpinner() {
        self.view.addSubview(self.spinner)
        self.spinner.center = self.view.center
        self.spinner.startAnimating()
    }
    
    private func hideSpinner() {
        self.spinner.removeFromSuperview()
    }
}

// MARK: PopularMoviesView
extension PopularMoviesViewController: PopularMoviesView {
    
    /// Update the list of movies being displayed buy appending.
    /// and performing the inserts without animation
    ///
    /// - Parameter popularMovies: list of Movie
    func updateList(_ popularMovies: [Movie]) {
        self.collectionView.refreshControl?.endRefreshing()
        self.popularMovies += popularMovies
        let nextIndexes = popularMovies.enumerated().map {  IndexPath(item: $0.offset, section: 0) }
        UIView.performWithoutAnimation { [weak self] in
            self?.collectionView.insertItems(at: nextIndexes)
            self?.hideSpinner()
        }
    }
    
    /// Refreshes the list of movies, reloads the collection view and hides the spinner
    /// also has the option so show a small label if new data is available
    ///
    /// - Parameters:
    ///   - movies: List of Movie
    ///   - showNewData: A small label to show when new data is avaiable
    func refreshList(_ movies: [Movie], showNewData: Bool) {
        if showNewData {self.showNewDataAlert()}
        self.collectionView.refreshControl?.endRefreshing()
        self.popularMovies = movies
        self.collectionView.reloadData()
        self.hideSpinner()
    }
    
    func showAlert(_ title: String?, message: String) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertViewController, animated: true, completion: nil)
    }
}

// MARK: UISearchBarDelegate
extension PopularMoviesViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {return}
        self.presenter?.searchForMovie(text)
    }
}

// MARK: UICollectionViewDataSourcePrefetching
extension PopularMoviesViewController: UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: { $0.row == self.popularMovies.count - 4 }) {
            self.showSpinner()
            self.presenter?.getNextPage(false)
        }
    }
}

// MARK: UICollectionViewDataSource
extension PopularMoviesViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.popularMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.idenitifer, for: indexPath) as? MovieCollectionViewCell else {
            fatalError("MovieCollectionViewCell was not created")
        }
        cell.imageLoader = self.imageLoader
        cell.setupCell(self.popularMovies[indexPath.row])
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension PopularMoviesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.movieSelected(self.popularMovies[indexPath.row], imageLoader:  self.imageLoader)
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension PopularMoviesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var noOfColumns: CGFloat = 2 // default for iPhone Portrait
        if UIDevice.current.userInterfaceIdiom == .pad {
            noOfColumns = UIDevice.isLandscape ? 5 : 4
        } else {
            noOfColumns = UIDevice.isPortrait ? 2 : 4
        }
        let screenWidth = self.view.safeAreaLayoutGuide.layoutFrame.size.width
        let cellWidth = (screenWidth / CGFloat(noOfColumns) - (noOfColumns * 2.5))
        return CGSize(width: cellWidth, height: cellWidth * 1.9)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.5
    }
}
