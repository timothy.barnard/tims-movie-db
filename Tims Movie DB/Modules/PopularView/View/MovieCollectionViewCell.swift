//
//  MovieCollectionViewCell.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    static var idenitifer = String(describing: self)
    
    // MARK: UIViews
    private var movieImageView = UIImageView.makeMovieRoundedEdges()
    private var titleLabel = UILabel.createSubHeading(2, textColor: .white)
    private var ratingLabel = UILabel.createSmall()
    
    internal var imageLoader: ImageLoader?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(code:) has not been implemented")
    }
    
    private func setupView() {
        let verticalStackView = UIStackView(frame: .zero)
        verticalStackView.axis = NSLayoutConstraint.Axis.vertical
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.distribution = .fill
        verticalStackView.spacing = 10
        verticalStackView.alignment = .center
        self.addSubview(verticalStackView)
        self.pinToEdges(verticalStackView)
        
        self.titleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.ratingLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        self.movieImageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        
        verticalStackView.addArrangedSubview(movieImageView)
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.addArrangedSubview(ratingLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor),
            
            ratingLabel.leadingAnchor.constraint(equalTo: verticalStackView.leadingAnchor),
            ratingLabel.trailingAnchor.constraint(equalTo: verticalStackView.trailingAnchor),
            ])
    }
    
    func setupCell(_ popularMovie: Movie) {
        if let posterPath = popularMovie.posterPath, let url = APIEndPoints.image(posterPath).url {
            self.imageLoader?.loadImage(from: url, retrievedImage: { [weak self] (image, _) in
                guard let movieImage = image else { return}
                self?.movieImageView.image = movieImage
            })
        }
        self.titleLabel.text = popularMovie.title
        self.ratingLabel.text = popularMovie.voteAverage.ratingStars
    }
}
