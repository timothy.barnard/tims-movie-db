//
//  MovieDetailInteractor.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class MovieDetailInteractor {

    // MARK: Properties
    weak var output: MovieDetailInteractorOutput?
}

extension MovieDetailInteractor: MovieDetailUseCase {
    
    /// Gets more information for the movie
    /// and retrieves the videos with two requests
    func getMovieDetails(_ id: Int) {
        let movieService = MovieService()
        var movie: MovieDetail?
        var movieVideos: [MovieVideo]?
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        movieService.getMovieDetails(id) { (_movie) in
            movie = _movie
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        movieService.getMovieVideos(id) { (_movieVideos) in
            movieVideos = _movieVideos
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            self?.output?.sendBackMovie(movie)
            self?.output?.sendBackMovieVideos(movieVideos)
        }
    }
}
