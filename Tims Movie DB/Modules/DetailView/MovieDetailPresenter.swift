//
//  MovieDetailPresenter.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

class MovieDetailPresenter {

    // MARK: Properties
    var view: MovieDetailView?
    var router: MovieDetailWireframe?
    var interactor: MovieDetailUseCase?
    
    var popularMovie: Movie?
    var movieVideos: [MovieVideo]?
    var imageLoader: ImageLoader?
}

extension MovieDetailPresenter: MovieDetailPresentation {
    
    func viewDidLoad() {
        if imageLoader == nil {
            self.imageLoader = ImageLoader()
        }
        // Updates the UI with the Movie object while retrieving extra data
        if let popularMovie = popularMovie, let id = popularMovie.id {
            self.interactor?.getMovieDetails(id)
            self.view?.displayMovie(popularMovie)
        }
    }
    
    func viewDidAppear() {}
    
    var videoURL: URL? {
        return self.movieVideos?.first?.youtubeURL
    }
}

extension MovieDetailPresenter: MovieDetailInteractorOutput {
    
    func sendBackMovie(_ movieDetail: MovieDetail?) {
        if let detail = movieDetail {
            self.popularMovie?.detail = detail
            self.view?.displayMovie(popularMovie!)
        }
    }
    
    func sendBackMovieVideos(_ movieVideos: [MovieVideo]?) {
        self.movieVideos = movieVideos
        if (movieVideos ?? []).isEmpty {
            self.view?.hideVideoPlayer()
        }
    }
}
