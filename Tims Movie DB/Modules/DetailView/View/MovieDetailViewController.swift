//
//  MovieDetailViewController.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import YoutubeDirectLinkExtractor

class MovieDetailViewController: UIViewController {

    // MARK: UIViews
    private lazy var backdropImageView = UIImageView.makeMovieImageView(true)
    private lazy var playButton: UIButton = {
        let button = UIButton.makePlayButton()
        button.addTarget(self, action: #selector(self.playButtonPressed), for: .touchUpInside)
        return button
    }()
    private lazy var posterImageView = UIImageView.makeMovieImageView()
    private lazy var titleLabel = UILabel.createTitle(-1, textColor: .red)
    private lazy var genreLabel = UILabel.createSubTitle(-1, textColor: .white)
    private lazy var releasedDateLabel = UILabel.createSubTitle(1, textColor: .white)
    private lazy var runTimeLabel = UILabel.createSubTitle(1, textColor: .white)
    private lazy var ratingLabel = UILabel.createSubTitle(1, textColor: .white)
    private lazy var overviewTextView = UITextView.createTextArea()
    
    // MARK: Properties
    var presenter: MovieDetailPresentation?
    let playerViewController = AVPlayerViewController()
    
    // MARK: View Constraints
    var backdropImageViewHeightConstraint: NSLayoutConstraint?
    var playbuttonConstraints: [NSLayoutConstraint] = []
    var posterTopConstraint: NSLayoutConstraint?

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        presenter?.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidAppear()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { [weak self] (_ ) in
            self?.setupBackdropImage()
            self?.setupPosterConstraint()
            self?.setupPlaybuttonConstraint()
        }, completion: nil)
        self.loadViewIfNeeded()
    }
    
    private func setupView() {
        self.navigationController?.navigationBar.setBarTransparent()
        self.view.backgroundColor = .black
        self.view.addSubview(backdropImageView)
        self.view.addSubview(posterImageView)
        self.view.addSubview(overviewTextView)
        self.view.addSubview(playButton)
        
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = 10
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(stackView)
        
        stackView.addArrangedSubview(self.titleLabel)
        stackView.addArrangedSubview(self.genreLabel)
        stackView.addArrangedSubview(self.releasedDateLabel)
        stackView.addArrangedSubview(self.runTimeLabel)
        stackView.addArrangedSubview(self.ratingLabel)
        
        self.backdropImageViewHeightConstraint = self.backdropImageView.heightAnchor.constraint(equalToConstant: self.view.frame.height * 0.3)
        self.backdropImageViewHeightConstraint?.isActive = true
        
        self.posterTopConstraint = self.posterImageView.topAnchor.constraint(equalTo: self.backdropImageView.bottomAnchor, constant: -20)
        self.posterTopConstraint?.isActive = true
        
        self.setupBackdropImage()
        
        NSLayoutConstraint.activate([
            self.backdropImageView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.backdropImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.backdropImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            
            self.playButton.heightAnchor.constraint(equalToConstant: 50),
            self.playButton.widthAnchor.constraint(equalToConstant: 50),
            
            self.posterImageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 5),
            self.posterImageView.widthAnchor.constraint(equalToConstant: 120),
            self.posterImageView.heightAnchor.constraint(equalToConstant: 180),
            
            stackView.topAnchor.constraint(equalTo: self.posterImageView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: self.posterImageView.trailingAnchor, constant: 5),
            stackView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            stackView.bottomAnchor.constraint(greaterThanOrEqualTo: self.posterImageView.bottomAnchor),
            
            self.overviewTextView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 15),
            self.overviewTextView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            self.overviewTextView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            ])
        self.setupPosterConstraint()
        self.setupPlaybuttonConstraint()
    }
    /// Configures the backdrop imageview with position and visible depending on orientation
    private func setupBackdropImage() {
        self.backdropImageViewHeightConstraint?.constant = UIDevice.current.orientation.isLandscape ? 0 : self.view.frame.height * 0.3
        self.backdropImageView.isHidden = UIDevice.current.orientation.isLandscape
    }
    
    /// Sets up contraints for the video play button, so when in landscape the button is on the right
    /// else in portrait the button is in the middle of the backdrop image view.
    private func setupPlaybuttonConstraint() {
        if !self.playbuttonConstraints.isEmpty {
            NSLayoutConstraint.deactivate(self.playbuttonConstraints)
            self.playbuttonConstraints.removeAll()
        }
        if UIDevice.current.orientation.isPortrait {
            let centerXConstraint = self.playButton.centerXAnchor.constraint(equalTo: self.backdropImageView.centerXAnchor)
            let centerYConstraint = self.playButton.centerYAnchor.constraint(equalTo: self.backdropImageView.centerYAnchor)
            self.playbuttonConstraints.append(contentsOf: [centerXConstraint, centerYConstraint])
        } else {
            let trailingConstraint = self.playButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -20)
            let topConstraint = self.playButton.bottomAnchor.constraint(equalTo: self.posterImageView.bottomAnchor, constant: -10)
            self.playbuttonConstraints.append(contentsOf: [trailingConstraint, topConstraint])
        }
        self.playbuttonConstraints.forEach { $0.isActive = true }
    }
    
    /// Sets up contraints for the poster, when in landscape the imageview is at the top left
    /// else in portrait, the image view is left just below the back drop image view.
    private func setupPosterConstraint() {
        if let constraint = self.posterTopConstraint {
            NSLayoutConstraint.deactivate([constraint])
        }
        if UIDevice.current.orientation.isPortrait {
            self.posterTopConstraint = self.posterImageView.topAnchor.constraint(equalTo: self.backdropImageView.bottomAnchor, constant: -20)
        } else {
            self.posterTopConstraint = self.posterImageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor)
        }
        self.posterTopConstraint?.isActive = true
    }
    
    /// Action for when the play button is pressed.
    /// First it uses YoutubeDirectLinkExtractor to extract the vidoe link from Youtube
    /// then uses AVPlayer to play the video
    @objc func playButtonPressed(_ sender: UIButton) {
        guard let videoURL = self.presenter?.videoURL else {return}
        let youtubeExtractor = YoutubeDirectLinkExtractor()
        youtubeExtractor.extractInfo(for: .url(videoURL), success: { [weak self] info in
            guard let vc = self else {return}
            let player = AVPlayer(url: URL(string: info.highestQualityPlayableLink!)!)
            vc.playerViewController.player = player
            NotificationCenter.default.addObserver(vc, selector: #selector(self?.videoHasFinished(_:)),
                                                   name: Notification.Name.AVPlayerItemDidPlayToEndTime,
                                                   object: vc.playerViewController.player?.currentItem)
            vc.present(vc.playerViewController, animated: true) {
                player.play()
            }
        }) { error in
            print(error)
            self.showAlert("error".localized, message: "error-message".localized)
        }
    }
    
    @objc func videoHasFinished(_ sender: Notification) {
        self.playerViewController.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        print("MovieDetailViewController has deinit")
    }
    
    private func showAlert(_ title: String?, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension MovieDetailViewController: MovieDetailView {
    
    func displayMovie(_ movie: Movie) {
        if let imagePath = movie.detail?.backdropPath, let url = APIEndPoints.image(imagePath).url {
            self.presenter?.imageLoader?.loadImage(from: url, retrievedImage: { [weak self] (image, _) in
                self?.backdropImageView.image = image
            })
        }
        if let posterPath = movie.posterPath, let url = APIEndPoints.image(posterPath).url {
            self.presenter?.imageLoader?.loadImage(from: url, retrievedImage: { [weak self] (image, _) in
                self?.posterImageView.image = image
            })
        }
        self.titleLabel.text = movie.title
        self.overviewTextView.text = movie.overview
        self.genreLabel.text = movie.detail?.labelGenre
        self.ratingLabel.text = movie.voteAverage.ratingStars
        if let date = movie.releaseDate {
            let formater = DateFormatter()
            formater.dateStyle = .medium
            self.releasedDateLabel.text = formater.string(from: date)
        }
        self.runTimeLabel.text = movie.detail?.runtime.minsToHoursMins
    }
    
    /// Hides the video if there are no videos available
    func hideVideoPlayer() {
        self.playButton.isHidden = true
    }
}
