//
//  MovieDetailContract.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

/// Protcol for the presenter to communicate with the view
protocol MovieDetailView {
    func displayMovie(_ movie: Movie)
    func hideVideoPlayer()
}

/// Protcol for the view to communicate with the presenter
protocol MovieDetailPresentation: class {
    func viewDidLoad()
    func viewDidAppear()
    var videoURL: URL? {get}
    var imageLoader: ImageLoader? {get}
}

/// Protcol for the presenter to communicate with the interactor
protocol MovieDetailUseCase: class {
    func getMovieDetails(_ id: Int)
}

/// Protcol for the interactor to communicate with the presenter
protocol MovieDetailInteractorOutput: class {
    func sendBackMovie(_ movie: MovieDetail?)
    func sendBackMovieVideos(_ movieVideos: [MovieVideo]?)
}

/// Protcol for the presenter to communicate with the router
protocol MovieDetailWireframe: class {}
