//
//  MovieDetailRouter.swift
//  Tims Movie DB
//
//  Created by Timothy on 21/06/2019.
//Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

class MovieDetailRouter {

    // MARK: Properties
    weak var viewController: UIViewController?

    // MARK: Static methods
    static func assembleModule(_ popularMovie: Movie, imageLoader: ImageLoader?) -> MovieDetailViewController {
        let view = MovieDetailViewController()
        let presenter = MovieDetailPresenter()
        let router = MovieDetailRouter()
        let interactor = MovieDetailInteractor()

        view.presenter =  presenter

        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.popularMovie = popularMovie
        presenter.imageLoader = imageLoader
        interactor.output = presenter
        
        router.viewController = view

        return view
    }
}

extension MovieDetailRouter: MovieDetailWireframe {}
