//
//  LocalMovieStore.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import CoreData

internal final class LocalMovieStore {
    
    private var context: NSManagedObjectContext!
    
    init(_ context: NSManagedObjectContext = CoreDataManager.shared.context) {
        self.context = context
    }
    
    /// Maps database MovieDB list to Movie list
    ///
    /// - Parameter list: [MovieDB]
    /// - Returns: [Movie]
    private func convertToMovieList(_ list: [MovieDB]) -> [Movie] {
        return list.map({ (movieDB) -> Movie in
            var movie = Movie()
            movie.id = Int(movieDB.id)
            movie.overview = movieDB.overview
            movie.title = movieDB.title
            movie.voteAverage = movieDB.vote_average as? Double
            movie.voteCount = movieDB.vote_count as? Int
            movie.posterPath = movieDB.poster_path
            movie.detail = MovieDetail(
                backdropPath: movieDB.backdrop_path,
                genres: nil, runtime: Int(movieDB.runtime),
                status: movieDB.status, genresStr: movieDB.genres)
            return movie
        })
    }
    
    /// Retrieves list of movies from the databsase
    ///
    /// - Parameter context: Context
    /// - Returns: Optional [MovieDB]
    private func getListOfDBMovies(_ context: NSManagedObjectContext) -> [MovieDB]? {
        let fetchRequest = MovieDB.fetchRequest() as NSFetchRequest<MovieDB>
        do {
            return try context.fetch(fetchRequest)
        } catch {
            print("Error retrieving movies: \(error)")
            return nil
        }
    }
    
    /// Saves list of Movie to the database.
    ///
    /// - Parameter movies: List of movies t save
    func saveMovieList(_ movies: [Movie]?) {
        movies?.forEach({ (movie) in
            let movieDB = MovieDB(context: self.context)
            movieDB.poster_path = movie.posterPath
            movieDB.id = Int32(movie.id ?? 0)
            movieDB.overview = movie.overview
            movieDB.release_date = movie.releaseDate as NSDate?
            movieDB.title = movie.title
            movieDB.vote_average = movie.voteAverage as? NSDecimalNumber
            movieDB.vote_count = movie.voteCount as? NSDecimalNumber
        })
        CoreDataManager.shared.saveContext(self.context)
    }
    
    /// Retrives list of Movie from the database
    ///
    /// - Returns: Movie list
    func getListOfMovies() -> [Movie]? {
        guard let dbMovies = self.getListOfDBMovies(self.context) else {return nil}
        return self.convertToMovieList(dbMovies)
    }
    
    /// Delete all movies from the database
    func removeAll() {
        let fetchRequest = MovieDB.fetchRequest() as NSFetchRequest<MovieDB>
        do {
            let results = try self.context.fetch(fetchRequest)
            for managedObject in results {
                self.context.delete(managedObject)
            }
        } catch {
            print("Error with deleting: \(error)")
        }
    }
    
    /// Searches for movie with a given title
    ///
    /// - Parameter name: title of the movie to search
    /// - Returns: List of Movie found
    func searchForMovie(_ name: String) -> [Movie]? {
        let fetchRequest = MovieDB.fetchRequest() as NSFetchRequest<MovieDB>
        fetchRequest.predicate = NSPredicate(format: "%K CONTAINS[c] %@", "title", name)
        do {
            let foundMovies = try self.context.fetch(fetchRequest)
            print("Found \(foundMovies.count) items")
            return self.convertToMovieList(foundMovies)
        } catch {
            print("Error with searching for movie: \(error)")
            return nil
        }
    }
    
    /// Updates a given movie Id with extra movie details
    ///
    /// - Parameters:
    ///   - id: id of the movie
    ///   - detail: Extra information which to save
    func updateMovie(with id: Int, detail: MovieDetail) {
        let idInt32 = Int32(id)
        let fetchRequest = MovieDB.fetchRequest() as NSFetchRequest<MovieDB>
        fetchRequest.predicate = NSPredicate(format: "id == %d", idInt32)
        do {
            let foundMovie = try self.context.fetch(fetchRequest).first
            if foundMovie != nil {
                foundMovie?.backdrop_path = detail.backdropPath
                foundMovie?.genres = (detail.genres ?? []).compactMap{ $0.name }.joined(separator: ", ")
                foundMovie?.status = detail.status
                foundMovie?.runtime = Int32(detail.runtime ?? 0)
                CoreDataManager.shared.saveContext(self.context)
            }
        } catch {
            print("Error updating movie with id: \(id) with error: \(error)")
        }
    }
}
