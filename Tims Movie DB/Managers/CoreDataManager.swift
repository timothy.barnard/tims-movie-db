//
//  CoreDataManager.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import CoreData

internal final class CoreDataManager {
    static let shared = CoreDataManager()
    private init() {} // Prevent from initialising a new class
    
    /// Database persistence container
    private lazy var perisistenceContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Tims_Movie_DB")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error {
                fatalError("Loading persistence stores error: \(error)")
            }
        })
        return container
    }()
    
    /// main context
    internal var context: NSManagedObjectContext {
        return CoreDataManager.shared.perisistenceContainer.viewContext
    }
    
    /// Private object context for background threads
    internal var privateManageObjectContext: NSManagedObjectContext {
        let manageObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        manageObjectContext.parent = self.context
        return manageObjectContext
    }

    @discardableResult
    internal func saveContext(_ context: NSManagedObjectContext = CoreDataManager.shared.context) -> Bool {
        if context.hasChanges {
            do {
                try context.save()
                return true
            } catch {
                fatalError("Save context error: \(error)")
            }
        }
        return false
    }
    
    internal func deleteObject(_ fetchRequest: NSFetchRequest<NSFetchRequestResult>,
                               context: NSManagedObjectContext = CoreDataManager.shared.context) {
        do {
            guard let object = try context.fetch(fetchRequest).first as? NSManagedObject else {return}
            context.delete(object)
        } catch {
            fatalError("Delete object error: \(error)")
        }
    }
}
