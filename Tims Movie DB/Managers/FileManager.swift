//
//  FileManager.swift
//  Tims Movie DB
//
//  Created by Timothy on 22/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation
import UIKit

internal final class ImageFileManager: NSObject {
    
    /// Returns back the apps document directory
    private var directoryPath: String? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.absoluteString
    }
    
    /// Saving image to file in the app document directory
    ///
    /// - Parameters:
    ///   - name: name of the file
    ///   - image: UImage in which to save
    func saveImageToFile(_ name: String, image: UIImage) {
        if let data =  image.pngData(), let directory = self.directoryPath {
            if let url = NSURL(fileURLWithPath: directory).appendingPathComponent("\(name).png") {
                do {
                    try data.write(to: url)
                } catch {
                    print(error)
                }
            }
        }
    }
    
    /// Retrieve image from document directory with name given
    ///
    /// - Parameter name: name of the image
    /// - Returns: path of the image if it exists
    func getImagPath(_ name: String) -> String? {
        guard let directory = self.directoryPath else {return nil}
        guard let url = NSURL(fileURLWithPath: directory).appendingPathComponent("\(name).png") else {return nil}
        if FileManager.default.fileExists(atPath: url.path) {
            return url.absoluteString
        }
        return nil
    }
}
