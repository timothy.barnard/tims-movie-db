//
//  ImageLoader.swift
//  Tims Movie DB
//
//  Created by Timothy on 23/06/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import UIKit

internal final class ImageLoader: NSObject {
    
    var cacheImages = NSCache<NSString, UIImage>()
    
    internal func loadImage(from url: URL, retrievedImage: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        if let cachedImage = cacheImages.object(forKey: url.path as NSString) {
            DispatchQueue.main.async {
                retrievedImage(cachedImage, nil)
            }
        } else {
            let imageName = url.fileName
            let fileManager = ImageFileManager()
            if let imagePath = fileManager.getImagPath(imageName) {
                DispatchQueue.main.async {
                    retrievedImage(UIImage(contentsOfFile: imagePath), nil)
                }
            }
            URLSession.shared.dataTask(with: url) { data, respsonse, error in
                guard let data = data, let mimeType = respsonse?.mimeType,  mimeType.hasPrefix("image"), let image = UIImage(data: data) else {
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    DispatchQueue.main.async {
                        retrievedImage(nil, error)
                    }
                    return
                }
                fileManager.saveImageToFile(imageName, image: image)
                DispatchQueue.main.async {
                    self.cacheImages.setObject(image, forKey: url.path as NSString , cost: 1)
                    retrievedImage(image, error)
                }
            }.resume()
        }
    }
}
